// pages/report_list/index.js
const app = getApp()
const fetch = require('../../utils/fetch')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    datas:[],
    page: 0,
    search_name:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.page = 0;
    this.data.datas = [];
    this.toSearchReportList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.page = 0;
    this.data.datas = [];
    this.toSearchReportList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
    * 查询举报列表
    */
  toSearchReportList: function () {
    var that = this;
    app.login(function (session) {
      if (session) {
        var oldDatas = that.data.datas;
        fetch('yggc/commonApi/reportList', { "wx_session": session, "page": that.data.page }, 'POST').then(function (result) {
          console.log(result, "reportList");
          var code = result.data.code;
          if (code == 200) {
            var newDatas = oldDatas.concat(result.data.data);;
            if (result.data.data.length > 0) {
              that.data.page += 1;
            }
            that.setData({
              datas: newDatas
            });
          } else if (code == 201) {
            wx.removeStorageSync('3rd_session_key');
          } else if (code == 410) {
            console.log('账号在其他地方登录，请重新登录');
          }
          wx.stopPullDownRefresh();
        });
      } else {
        // session过期
      }

    });
  },
  toReport:function(){
    wx.navigateTo({
      url: '../report/index',
    })
  },
  toReportProcess:function(e){
    var id = e.currentTarget.dataset.index;
    console.log(e);
    // 跳转举报处理页
    wx.navigateTo({
      url: '../report_process/index?id='+id,
    })
  }
})