// pages/report_panel/index.js
// 举报面板
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.login();
  },

  toReport:function(){
    console.log('toReport')
    wx.navigateTo({
      url: '../report/index'
    })
  },

  toFeedback:function(){
    console.log('toReportFeedback')
    wx.navigateTo({
      url: '../report_feedback/index',
    })
  }


})