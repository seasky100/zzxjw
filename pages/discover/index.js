// pages/discover/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    messages:[]
  },

  testMessages:{
    title:"标题",
    summary:"副标题",
    image:"../../images/ad/ad_1.jpeg",
    date:"2017-09-09 12:09:08"
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        messages:[this.testMessages]
      });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log("onPullDownRefresh");
    var that = this;
    setTimeout(function(){
      var msg = that.testMessages;
      msg.image = "../../images/ad/ad_" + (that.data.messages.length %3 +1)+".jpeg";
      var msgs = that.data.messages;
      msgs.unshift(msg);
      that.setData({
        messages: msgs
      })
      wx.stopPullDownRefresh();
    },1000)
 
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

})