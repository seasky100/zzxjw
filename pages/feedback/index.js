// pages/feedback/index.js 投诉反馈
var tempY=0;
const app = getApp()
const recorderManager = wx.getRecorderManager();

const options = {
  duration: 10000,
  sampleRate: 44100,
  numberOfChannels: 1,
  encodeBitRate: 192000,
  format: 'mp3',
  frameSize: 50
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isTextBox:true,
    voiceBtnText:"按住开始录音",
    isRecording:false,
    isCancle:false,
    userInfo:null,
    // 显示数据
    datas:[
      // {
      // "user_type":0,
      // "content":"这是用户的一条消息",
      // "content_type":0,
      // "create_time":"2017-12-15 08:15:22",
      
      // }, 
      // {
      // "user_type": 1,
      // "content": "这是客服的反馈",
      // "content_type": 0,
      // "create_time": "2017-12-15 08:15:22",
      // }, 
      // {
      //   "user_type": 1,
      //   "content": "这是客服的反馈",
      //   "content_type": 0,
      //   "create_time": "2017-12-15 08:15:22",
      // },
      // {
      //   "user_type": 0,
      //   "content": "这是用户的一条消息",
      //   "content_type": 1,
      //   "create_time": "2017-12-15 08:15:22",
      //   "audioInfo": {
      //     'id':1,
      //     'src':'',
      //     'duration':100,
      //   }
      // },

      {
      "user_type":0,
      "content":"我要投诉，我的煤炭补助没有收到",
      "content_type":0,
      "create_time":"2017-12-15 08:15:22",

      }, 
      {
      "user_type": 1,
      "content": "您好，您的投诉我们已收到，近期会找相关部门了解情况，后续结果请继续关注本平台！",
      "content_type": 0,
      "create_time": "2017-12-15 08:25:41",
      }, 
      ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    recorderManager.onStop(this.onRecordStop);
    var isLogin = app.isLogin();
 
    var that = this;

    if (isLogin) {
      app.getUserInfo(
        function (res) {
          console.log(res, "onLoad");
          console.log(isLogin);
          that.setData({
            userInfo: res
          });
        }
      );
    }
  },
  onRecordStop:function(res){
    console.log(res);
    if (this.data.isCancle) {
      //取消了录音
      wx.showToast({
        title: '取消录音',
        image: '../../images/common/voice_cancle.png',
        duration: 1000
      })
    } else {
      // 结束了录音
      wx.showToast({
        title: '录音数据是：'+res.tempFilePath,
        duration: 1000
      })

      // 1.存储到本地，
      // 2.发送语音到服务端
      // 3.
      var data = {
        "user_type": 0,
        "content": "这是用户的一条消息",
        "content_type": 1,
        "create_time": "2017-12-15 08:15:22",
        "audioInfo": {
          'id': 1,
          'src': res.tempFilePath,
          'duration': res.duration,
        }
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  changeInputType:function(){
    var isTextBox = !this.data.isTextBox;
    this.setData({
      isTextBox:isTextBox
    })
  },
  startRecord:function(e){
    recorderManager.start(options);
      tempY = e.touches[0].pageY;
      wx.showLoading({ title:"正在录音"});
      this.setData({
        voiceBtnText:"松开结束",
        isRecording:true,
        isCancle:false
      });
  },
  endRecord:function(e){
    wx.hideLoading();
   
    this.setData({
      voiceBtnText: "按住开始录音",
      isRecording: false
    });
    recorderManager.stop();
    // console.log(recordInfo, 'recordInfo);
    
  },
  processRecording:function(e){
    var currY = e.touches[0].pageY;
    if(tempY - currY >100){
      this.setData({
        voiceBtnText: "松开取消录音",
        isCancle: true,
      });
    }else{
      this.setData({
        voiceBtnText: "松开结束",
        isCancle: false,
      });
    }
    console.log(e);
  },

})