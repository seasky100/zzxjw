// pages/report_detail/index.js
const app = getApp();
const fetch = require('../../utils/fetch')
const util = require('../../utils/util');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    imagesUrl:[],
    imageMap:{},
    isReal:true,
    QIndex:0,
    SubQIndex:0,
    // 问题类型
    questionType: [
        {
          id: 0,
          name: "请选择"
        },
        {
          "id": 1,
          "name": "违反政治纪律行为"
        },
        {
          "id": 2,
          "name": "违反组织纪律行为"
        },
        {
          "id": 3,
          "name": "违反廉洁纪律行为"
        },
        {
          "id": 4,
          "name": "违反群众纪律行为"
        },
        {
          "id": 5,
          "name": "违反工作纪律行为"
        },
        {
          "id": 6,
          "name": "违反生活纪律行为"
        },
        {
          "id": 7,
          "name": "涉法行为"
        }
    ],
    questionSubType:[
      {
        id: 0,
        name: "请选择"
      },
    ],
    politicalIndex:0,
    // 政治面貌
    political:[
      {
        id: "0",
        name: "请选择"
      },
      {
        id: "59",
        name: "中国共产党党员"
      },
      {
        id: "60",
        name: "中共预备党员"
      },
      {
        id: "61",
        name: "共青团员"
      },
      {
        id: "62",
        name: "民主党派人士"
      },
      {
        id: "63",
        name: "无党派民主人士"
      },
      {
        id: "64",
        name: "群众"
      }
    ],
    reportLevelIndex:0,
    beReportLevelIndex:0,
    region:["内蒙古自治区","乌兰察布市","卓资县"],
    // 级别
    level: [
            {
              id: "0",
              name: "请选择"
            },
            // {
            //   id: "65",
            //   name: "正省部级"
            // },
            // {
            //   id: "126",
            //   name: "副省部级"
            // },
            // {
            //   id: "66",
            //   name: "正厅局级"
            // },
            // {
            //   id: "127",
            //   name: "副厅局级"
            // },
            {
              id: "67",
              name: "正县处级"
            },
            {
              id: "128",
              name: "副县处级"
            },
            {
              id: "68",
              name: "正乡科级"
            },
            {
              id: "129",
              name: "副乡科级"
            },
            {
              id: "69",
              name: "一般干部"
            },
            {
              id: "70",
              name: "军队"
            },
            {
              id: "71",
              name: "金融机构"
            },
            {
              id: "72",
              name: "一般企业"
            },

            {
              id: "73",
              name: "事业"
            },
            {
              id: "74",
              name: "农村"
            },
            {
              id: "75",
              name: "其他"
            }
          ]
  
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onCheckReal:function(){
    var r = !this.data.isReal;
    this.setData({
      isReal:r
    })
  },
  bindPoliticalPickerChange:function(e){
    console.log("bindPoliticalPickerChange",e);
    var index = e.detail.value;
    this.setData({
      "politicalIndex":index
    });
  },
  bindReportLevelChange:function(e){
    var index = e.detail.value;
    this.setData({
      "reportLevelIndex": index
    });
  },
  bindBeReportLevelChange:function(e){
    var index = e.detail.value;
    this.setData({
      "beReportLevelIndex": index
    });
  },
  // 弃用
  bindQChange:function(e){
    var index = e.detail.value;
    var objItemValue ='';
    var v = this.data.questionType[index].name;
    var reportLevel = this.data.level[this.data.beReportLevelIndex].name;
    
    if (v == '违反政治纪律行为') {
      objItemValue = '54|公开发表危害党的言论,55|参加反对党和政府的活动或组织,56|在党内搞团团伙伙,57|妨碍党和国家方针政策实施,58|对抗组织审查,59|组织参加迷信活动,60|叛逃及涉外活动中损害党和国家利益,61|无原则一团和气和违反政治规矩';
    } else if (v == '违反组织纪律行为') {
      if (reportLevel == '农村' || reportLevel == '其他') {
        objItemValue = '62|违反民主集中制原则,63|不按要求请示报告有关事项,64|违规组织参加老乡会校友会战友会,65|侵犯党员权利,66|在投票和选举中搞非组织活动,68|在人事劳动工作中违规谋利,69|违规发展党员,70|违规办理出国证件和在境外脱离组织';
      } else {
        objItemValue = '62|违反民主集中制原则,63|不按要求请示报告有关事项,64|违规组织参加老乡会校友会战友会,65|侵犯党员权利,66|在投票和选举中搞非组织活动,67|违反干部选拔任用规定,68|在人事劳动工作中违规谋利,69|违规发展党员,70|违规办理出国证件和在境外脱离组织';
      }
    } else if (v == '违反廉洁纪律行为') {
      if (reportLevel == '一般干部' || reportLevel == '农村' || reportLevel == '其他') {
        objItemValue = '71|权权交易和纵容特定关系人以权谋私,72|违规接受礼品礼金宴请服务,73|违规操办婚丧喜庆事宜,74|违规从事营利活动,76|违规占有使用公私财物,77|违规参与公款宴请消费,78|违规自定薪酬和发放津贴补贴奖金,79|公款旅游,80|违反公务接待管理规定,81|违反公务用车管理规定,82|违反会议活动管理规定,83|违反办公用房管理规定,84|权色钱色交易,85|其他违反廉洁纪律行为';
      } else {
        objItemValue = '71|权权交易和纵容特定关系人以权谋私,72|违规接受礼品礼金宴请服务,73|违规操办婚丧喜庆事宜,74|违规从事营利活动,75|违反工作生活待遇规定,76|违规占有使用公私财物,77|违规参与公款宴请消费,78|违规自定薪酬和发放津贴补贴奖金,79|公款旅游,80|违反公务接待管理规定,81|违反公务用车管理规定,82|违反会议活动管理规定,83|违反办公用房管理规定,84|权色钱色交易,85|其他违反廉洁纪律行为';
      }
    } else if (v == '违反群众纪律行为') {
      objItemValue = '86|侵害群众利益,87|漠视群众利益,88|侵犯群众知情权,89|其他违反群众纪律行为';
    } else if (v == '违反工作纪律行为') {
      if (reportLevel == '一般干部' || reportLevel == '农村' || reportLevel == '其他') {
        objItemValue = '90|主体责任落实不力,93|泄露扩散窃取私存党的秘密,94|违反考试录取工作规定,95|其他违反工作纪律行为';
      } else {
        objItemValue = '90|主体责任落实不力,91|违规干预市场经济活动,92|违规干预执纪执法司法活动,93|泄露扩散窃取私存党的秘密,94|违反考试录取工作规定,95|其他违反工作纪律行为';
      }
    } else if (v == '违反生活纪律行为') {
      objItemValue = '96|生活奢靡,97|不正当性关系,98|其他违反生活纪律行为';
    } else if (v == '涉法行为') {
      objItemValue = '99|贪污贿赂行为,100|失职渎职行为,101|破坏社会主义市场经济秩序行为,102|侵犯人身权利民主权利行为,103|妨害社会管理秩序行为,104|其他涉法行为';
    }
    console.log(objItemValue);
    var subQuestion = [];
    var tempArr = objItemValue.split(',');
    subQuestion.push({
      "id": 0,
      "name": "请选择"
    });
    for(var i=0;i<tempArr.length;i++){
      var temp = tempArr[i].split('|');
      subQuestion.push({
        "id":temp[0],
        "name":temp[1]
      })
    }
    this.setData({
      "QIndex": index,
      "SubQIndex": 0,
      "questionSubType":subQuestion
    });
  }, 
  // 弃用
  bindSubQChange: function (e) {
    var index = e.detail.value;
    this.setData({
      "SubQIndex": index
    });
  },
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },
  processValues:function(values){
    var r = {};
    for(var key in values){
      if(values[key] == "请选择"){
        r[key] = "";
      }else{
        r[key] = values[key];
      }
    }
    return r;
  },
  toReport:function(e){
    console.log('toReport',e);
    var values = e.detail.value;
    if (this.validate(values)){
      values = this.processValues(values);
      if (this.data.imagesUrl.length >0){
        var imageNames = [];
        for(var i=0;i<this.data.imagesUrl.length;i++){
          var path = this.data.imagesUrl[i];
          if(this.data.imageMap[path]){
            imageNames.push((this.data.imageMap[path]));
          }
        }
        if(imageNames.length>0){
          values['imageNames'] = imageNames;
        }
      }
      app.login(function (session) {
        if (session) {
          values["wx_session"] = session;
          wx.showLoading({
            title: '正在上报举报信息..'
          })
          fetch('yggc/commonApi/report', values, 'POST').then(function (result) {
            console.log(result, "report");
            var code = result.data.code;
            if (code == 200) {
              // 弹出提示，举报成功，返回主页。
              wx.showModal({
                title: '举报信息提示',
                content: '举报信息已接收，请您确保联系方式畅通，工作人员会在汇总信息后主动与您联系',
                showCancel: false,
                confirmText: "我知道了",
                complete: function () {
                  console.log("complete");
                  wx.navigateBack({
                    delta: 2
                  })
                }
              })
              return;
            } else if (code == 201) {
              wx.removeStorageSync('3rd_session_key');
              wx.showModal({
                title: '举报信息提示',
                content: '用户session已过期，请重试！',
                showCancel: false,
                confirmText: "我知道了",
              })
              return;
            } else if (code == 410) {
              wx.showModal({
                title: '举报信息提示',
                content: '账号在其他地方登录，请重新登录！',
                showCancel: false,
                confirmText: "我知道了",
              })
              return;
            }

          });
        }else {

        }
      });
      
    }
  },
  

  validate:function(values){
    if (this.data.isReal){
       if( values.rName ==""){
          this.showErrorTips("举报人姓名必填！");
          return false;
       }
       if (!util.isValidityIdCard(values.rIdCard)){
         this.showErrorTips("身份证号有误！");
         return false;
       }
       if(values.rPhone == ''){
         this.showErrorTips("联系方式必填！");
         return false;
       }
       if(values.rAddress == ''){
         this.showErrorTips("现居住地址必填！");
         return false;
       }
    }
    // 被举报人信息
    if (values.brName == '') {
      this.showErrorTips("被举报人姓名必填！");
      return false;
    }
    if (values.brDepartment == '') {
      this.showErrorTips("被举报人单位必填！");
      return false;
    }
    if (values.brJob == '') {
      this.showErrorTips("被举报人职务必填！");
      return false;
    }
    // if (values.brLevel == '请选择') {
    //   this.showErrorTips("被举报人级别必填！");
    //   return false;
    // }
    // 
    if (values.title == '') {
      this.showErrorTips("举报正文标题必填！");
      return false;
    }
    // if (values.QType == '请选择') {
    //   this.showErrorTips("请选择问题类别");
    //   return false;
    // }
    // if (values.SubQType == '请选择') {
    //   this.showErrorTips("请选择问题细类");
    //   return false;
    // }
    if(values.content == ''){
      this.showErrorTips("举报主要问题必填！");
      return false;
    }
    return true;
  },

  showErrorTips:function(msg){
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false,
      confirmText: "我知道了"
    })
  },
  addImages:function(){
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePath = res.tempFilePaths[0];
        that.data.imagesUrl.push(tempFilePath);
        console.log(res);
        that.setData({
          imagesUrl: that.data.imagesUrl
        })
        that.uploadImages(tempFilePath);
      }
    })
  },
  removeImage:function(e){
    console.log(e,"removeImage");
    var index = e.currentTarget.dataset.index;
    this.data.imagesUrl.splice(index,1);
    console.log(this.data.imagesUrl.length, "removeImage");

    this.setData({
      imagesUrl: this.data.imagesUrl
    });
  },

  /**
   * 图片上传完成后，返回图片完整路径，或图片名称信息。
   * 
   */
  uploadImages: function (path) {
    var url = app.getBaseUrl() +"yggc/commonApi/uploadReportImage";
    var that = this;
    wx.uploadFile({
      url: url, //仅为示例，非真实的接口地址
      filePath: path,
      name: 'file',
      success: function (res) {
        console.log(res);
        var data = JSON.parse(res.data);
        if(data.code == 200){
          var name = data.data.fileName;
          that.data.imageMap[path] = name;
        }
        return;
        
      }
    })
  },

  

})