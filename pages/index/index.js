// pages/index/index.js
const app = getApp()
const fetch = require('../../utils/fetch')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currTabIndex: 1,
    swiperDatas: [],
    region_id: '150921',
    towns: [],
    newsDatas: [],
    newsDatasRecord: { "datas": [], "page": 0, "isLoaded": false },
    hasNews: false,
    menus:[
      {
        "title":"党务公开",
        "clickUrl":"/pages/search_news/index",
        "icon":"/images/grid/dangwu.jpg",
        "params":"type=0"
      },
      {
        "title": "政务公开",
        "clickUrl": "/pages/search_news/index",
        "icon": "/images/grid/zhengwu.jpg",
        "params": "type=1"
      },
      {
        "title": "财务公开",
        "clickUrl": "/pages/search_news/index",
        "icon": "/images/grid/chaiwu.jpg",
        "params": "type=2"
      },
      {
        "title": "办事指南",
        "clickUrl": "/pages/search_news/index",
        "icon": "/images/grid/huimin.jpg",
        "params":"tags=惠民政策"
      },
      {
        "title": "脱贫攻坚",
        "clickUrl": "/pages/search_news/index",
        "icon": "/images/grid/fupin.jpg",
        "params": "tags=扶贫政策"
      },
      {
        "title": "在线举报",
        "clickUrl": "/pages/report_panel/index",
        "icon": "/images/grid/jubao.jpg",
        "params": ""
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    fetch('yggc/commonApi/getRegionList', { "pCode": this.data.region_id }).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {
        that.setData({
          towns: result.data.data,
        });
      }
    });
    this.loadLocalNewsInfos();
    fetch('yggc/commonApi/getBanners').then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {
        that.setData({
          swiperDatas: result.data.data,
        });
      }
    });

  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '廉政便民服务快车道',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadLocalNewsInfos();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  loadLocalNewsInfos: function () {
    var that = this;
    // 原数据
    var oldDatas = this.data.newsDatasRecord;

    fetch('yggc/commonApi/searchNews', { "isImportant":1, 'page': oldDatas.page }).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {
        // 原type=1的数据
        var newDatasRecord = oldDatas;
        // 追加新数据
        newDatasRecord.datas = newDatasRecord.datas.concat(result.data.data);
        console.log(result.data.data.length);
        if (result.data.data.length > 0) {
          newDatasRecord.page += 1;
        }
        newDatasRecord.isLoaded = true;
        console.log(newDatasRecord, 'newDatasRecord');
        //存储该记录
        that.data.newsDatasRecord = newDatasRecord;
        // 重新渲染页面
        that.setData({
          newsDatas: newDatasRecord.datas,
          hasNews: newDatasRecord.datas.length > 0
        });
      }
    });
  },



  toNewsItemDetail: function (e) {
    console.log("toNewsItemDetail", e);
    var id = e.detail.id;
    // 传递文章id
    wx.navigateTo({
      url: '../news_detail/index?id=' + id,
    })
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    var index = e.detail.value;
    var name = this.data.towns[index].name;
    var id = this.data.towns[index].region_code;
    console.log(name);

    wx.navigateTo({
      url: '../town/index?id=' + id + '&name=' + name,
    })

  },
  toUserCenter: function () {
    wx.navigateTo({
      url: '../usercenter/index'
    })
  },
  toReportList: function () {
    wx.navigateTo({
      url: '../report/index'
    })
  },
  onClickGridItem:function(e){
    console.log("onClickGridItem",e)
    var index = e.currentTarget.dataset.index;
    var path = this.data.menus[index].clickUrl;
    var params = this.data.menus[index].params
    wx.navigateTo({
      url: path+"?"+params,
    })
  },
  toSearchNews:function(){
    wx.navigateTo({
      url: "/pages/search_news/index",
    })
  },
  toMoreLocal:function(){
    wx.navigateTo({
      url: "/pages/search_news/index?isImportant=1",
    })
  }
})