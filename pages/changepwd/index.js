// pages/changepwd/index.js
const app = getApp()
const fetch = require('../../utils/fetch')
const util = require('../../utils/util');
const md5 = require('../../utils/md5');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  toSetPwd:function(e){
    console.log(e);
    var id = e.detail.value.idcard;
    var newpwd = e.detail.value.newpwd;
    var oldpwd = e.detail.value.oldpwd;
    var renewpwd = e.detail.value.renewpwd;


    console.log('form发生了submit事件，携带数据为：', e.detail.value);
    var flag = util.isValidityIdCard(id);
    if (!flag) {
      wx.showToast({
        title: '身份证号有误',
        image: '../../images/common/error.png'
      });
      return;
    }
    if (newpwd == '' || oldpwd == '' || renewpwd == '') {
      wx.showToast({
        title: '密码不能为空',
        image: '../../images/common/error.png'
      });
      return;
    }
    if (newpwd.length < 6 || oldpwd.length < 6 || renewpwd.length < 6) {
      wx.showToast({
        title: '密码不能少于6位',
        image: '../../images/common/error.png'
      });
      return;
    }
    if(newpwd != renewpwd){
      wx.showToast({
        title: '确认密码有误！',
        image: '../../images/common/error.png'
      });
      return;
    }
    newpwd = md5.hexMD5(newpwd);
    oldpwd = md5.hexMD5(oldpwd);

    // pwd = md5.hexMD5(pwd);
    wx.showLoading({
      title: '修改密码中...',
    })

    app.login(function (session) {
      if (session) {
        fetch('yggc/commonApi/changePwd', { "idcard": id, "newpwd": newpwd, "oldpwd": oldpwd, "wx_session": session }, "POST").then(function (result) {
          console.log(result);
          var code = result.data.code;
          if (code == 200) {
            wx.showToast({
              title: '修改成功',
            });
            app.globalData.isLogin = false;
            wx.setStorageSync("idcard", '');
            app.globalData.idcard = '';
            setTimeout(function () {
              wx.navigateBack();
            }, 1000)
          } else if (code == 201) {
            wx.removeStorageSync('3rd_session_key');
            wx.showToast({
              title: 'session到期',
              image: '../../images/common/error.png'
            });
          }
          else if (code == 400) {
            wx.showModal({
              title: '提示',
              content: result.data.data,
              showCancel: false,
              confirmText: "我知道了"
            })
          }
        }, function (result) {
          wx.showToast({
            title: '连接不通畅',
            image: '../../images/common/error.png'
          });
        });
      } else {
        wx.showModal({
          title: '提示',
          content: "网络暂时不通畅，请稍后再试！",
          showCancel: false,
          confirmText: "我知道了"
        })
      }

    });
  }
})