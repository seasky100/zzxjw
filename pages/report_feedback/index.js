// pages/report_feedback/index.js
const app = getApp()
const fetch = require('../../utils/fetch')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    datas: [],
    page: 0,
    tips:"暂时没有举报记录"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.page = 0;
    this.data.datas = [];
    this.toSearchReportFeedbackList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.page = 0;
    this.data.datas = [];
    this.toSearchReportFeedbackList();
  },
  /**
      * 查询举报列表
      */
  toSearchReportFeedbackList: function () {
    var that = this;
    app.login(function (session) {
      if (session) {
        var oldDatas = that.data.datas;
        fetch('yggc/commonApi/reportFeedbackList', { "wx_session": session, "page": that.data.page }, 'POST').then(function (result) {
          console.log(result, "reportFeedbackLists");
          var code = result.data.code;
          if (code == 200) {
            var newDatas = oldDatas.concat(result.data.data);;
            if (result.data.data.length > 0) {
              that.data.page += 1;
            }
            that.setData({
              datas: newDatas
            });
          } else if (code == 201) {
            wx.removeStorageSync('3rd_session_key');
            console.log('下拉刷新重试');
            that.setData({
              "tips":"网络错误，请下拉刷新一下！"
            })
          } else if (code == 410) {
            console.log('账号在其他地方登录，请重新登录');
          }
          wx.stopPullDownRefresh();
        });
      } else {
        // session过期
      }

    });
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})