// pages/notice/index.js 公告
const app = getApp()

const fetch = require('../../utils/fetch')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    noticeDatas: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    fetch('yggc/commonApi/getNotices', { "idcard": app.globalData.idcard }, 'POST').then(function (result) {
      console.log(result, "getNotices");
      var code = result.data.code;
      if (code == 200) {
        that.setData({
          noticeDatas: result.data.data,
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  toNoticeItemDetail:function(e){
    console.log("toNoticeItemDetail", e);
    var article_id = e.detail.article_id;
    var notice_id = e.detail.id;
    var datas = this.data.noticeDatas;
    for(var i=0;i<datas.length;i++){
      var d = datas[i];
      if (notice_id == d.id){
        datas[i].isRead = true;
        break;
      }
    }
    this.setData({
        noticeDatas:datas
    });
    fetch('yggc/commonApi/beNoticeRead',
      { "idcard": app.globalData.idcard, "notice_id": notice_id }, 'POST');
    // 传递文章id
    wx.navigateTo({
      url: '../notice_detail/index?id=' + article_id,
    })
  }
})