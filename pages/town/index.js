// pages/test/index.js
const app = getApp()
const fetch = require('../../utils/fetch.js')
Page({


  /**
   * 页面的初始数据
   */
  data: {
    currTabIndex:1,
    swiperDatas: [],
    newsDatas: [],
    newsDatasRecord: [
      { "datas": [], "page": 0, "isLoaded":false },
      { "datas": [], "page": 0, "isLoaded": false },
      { "datas": [], "page": 0, "isLoaded": false }
    ],
    hasNews: false,
    region_id: '',
    villages:[],
    pageName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    var id = options.id;
    this.data.region_id = id;
    this.data.pageName = options.name;
    wx.setNavigationBarTitle({
      title: options.name,
    })
    var that = this;
    fetch('yggc/commonApi/getRegionList', { "pCode": this.data.region_id }).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {
        that.setData({
          villages: result.data.data,
        });
      }
    });

    this.loadNewsInfos(this.data.currTabIndex);
    fetch('yggc/commonApi/getBanners', { "code": this.data.region_id}).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {
        that.setData({
          swiperDatas: result.data.data,
        });
      }
    });
  

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: this.data.pageName,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadNewsInfos(this.data.currTabIndex);
  },

  loadNewsInfos: function (news_type) {
    var that = this;
    // 原数据
    var oldDatas = this.data.newsDatasRecord[news_type];

    fetch('yggc/commonApi/getNews', { "code": this.data.region_id, "type": news_type, 'page': oldDatas.page }).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {

        // 原type=1的数据
        var newDatasRecord = oldDatas;
        // 追加新数据
        newDatasRecord.datas = newDatasRecord.datas.concat(result.data.data);

        if (result.data.data.length > 0) {
          newDatasRecord.page += 1;
        }
        newDatasRecord.isLoaded = true;
        console.log(newDatasRecord, 'newDatasRecord');
        //TODO,存储分页信息

        //存储该记录
        that.data.newsDatasRecord[news_type] = newDatasRecord;
        // 重新渲染页面
        that.setData({
          newsDatas: newDatasRecord.datas,
          hasNews: newDatasRecord.datas.length > 0
        });
      }
    });
  },

  toNewsItemDetail: function (e){
    console.log("toNewsItemDetail", e);
    var id = e.detail.id;
    // 传递文章id
    wx.navigateTo({
      url: '../news_detail/index?id=' + id,
    })
  },
  changeTabIndex:function(e){
    var index = e.detail.index;
    this.data.currTabIndex = index;
    var oldDatas = this.data.newsDatasRecord[index];
    if (oldDatas.isLoaded) {
      this.setData({
        newsDatas: oldDatas.datas,
        hasNews: oldDatas.datas.length > 0
      });
    } else {
      this.loadNewsInfos(index);

    }
  },
  bindPickerChange:function(e){
    console.log(e,"bindPickerChange1");
    var index = e.detail.value;
    var id = this.data.villages[index].region_code;
    var name = this.data.villages[index].name;
    console.log(name,"bindPickerChange1");
    var nextName = this.data.pageName + "-" + name;
    console.log(nextName);
    wx.navigateTo({
      url: '../village/index?id=' + id + '&name=' + nextName,
    })

  },
  toUserCenter: function () {
    wx.navigateTo({
      url: '../usercenter/index'
    })
  },
  toReportList: function () {
    wx.navigateTo({
      url: '../report/index'
    })
  }
})