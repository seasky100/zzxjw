// pages/subsidy/index.js 补贴
const app = getApp()
const fetch = require('../../utils/fetch');
const util = require('../../utils/util');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    currYear:'2017',
    general:{
      "history":{"price":"--","tips":"暂无数据"},
      "currYear": { "price": "--", "tips": "暂无数据"}
    },
    datas:[],
    page:0
  },

  // general: {
  //   "history": { "price": 1098.3, "tips": "2017年12月 ~ 至今" },
  //   "currYear": { "price": 408.3, "tips": "2017年1月 ~ 至今" }
  // }
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var currYear = util.formatYear(new Date());
    console.log(currYear);
    this.setData({
      datas: [],
      page: 0,
      currYear: currYear,
    });
    this.toSearchForYear(currYear);

    this.getGeneralData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log("onPullDownRefresh");
    this.getGeneralData();
    this.setData({
      datas: [],
      page: 0,
      general: {
        "history": { "price": "--", "tips": "暂无数据" },
        "currYear": { "price": "--", "tips": "暂无数据" }
      },
    });
    this.toSearchForYear(this.data.currYear);

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e)
    var data = e.detail.value;
    if (this.data.currYear == data){

    }else{
      this.setData({
        datas:[],
        page:0,
        currYear: data,
      });
      this.toSearchForYear(data);
    }
    
    
  },

  /**
   * 获取概要信息
   */
  getGeneralData:function(){
    var that = this;
    app.login(function (session){
      if (session) {
        fetch('yggc/commonApi/getSubsidyGeneralInfos', {"wx_session": session }, 'POST').then(function (result) {
          console.log(result, "getGeneralData ");
          var code = result.data.code;
          if (code == 200) {
            that.setData({
              general:result.data.data
            });
          }else if (code == 201){
            wx.removeStorageSync('3rd_session_key');
          }else if(code == 400){
            wx.showToast({
              title: "请求失败",
            })
          }else if(code == 410){
            console.log('账号在其他地方登录，请重新登录');
          }
          wx.stopPullDownRefresh()
        });
      }else{
        wx.removeStorageSync('3rd_session_key');
        wx.showToast({
          title: 'session过期,请下拉再试！',
        })
        // session过期
        wx.stopPullDownRefresh()
      }
      
    });
   
  },

  /**
   * 查询补贴列表
   */
  toSearchForMouth:function(date){
    var that = this;
    app.login(function (session) {
      if (session) {
        var oldDatas = that.data.datas;
        fetch('yggc/commonApi/getSubsidyMouthInfos', { "wx_session": session, "date": date, "page": that.data.page }, 'POST').then(function (result) {
          console.log(result, "getGeneralData");
          var code = result.data.code;
          if (code == 200) {
            var newDatas = oldDatas.concat(result.data.data);;
            if (result.data.data.length > 0) {
              that.data.page += 1;
            }
            that.setData({
              datas: newDatas
            });
            
          } else if (code == 201) {
            wx.removeStorageSync('3rd_session_key');
          } else if (code == 410) {
            console.log('账号在其他地方登录，请重新登录');
          }
        });
      } else {
        // session过期
      }

    }); 
  },
  /**
  * 查询补贴列表
  */
  toSearchForYear: function (date) {
    var that = this;
    app.login(function (session) {
      if (session) {
        var oldDatas = that.data.datas;
        fetch('yggc/commonApi/getSubsidyYearInfos', { "wx_session": session, "date": date, "page": 0 }, 'POST').then(function (result) {
          console.log(result, "toSearchForYear");
          var code = result.data.code;
          if (code == 200) {
            var newDatas = oldDatas.concat(result.data.data);;
            if (result.data.data.length > 0) {
              that.data.page += 1;
            }
            that.setData({
              datas: newDatas
            });

          } else if (code == 201) {
            wx.removeStorageSync('3rd_session_key');
          } else if (code == 410) {
            console.log('账号在其他地方登录，请重新登录');
            wx.showModal({
              title: '账号异常提醒',
              content: '账号在其他地方登录，请重新登录',
              success: function (res) {
                  app.toLoginOut();
                  console.log(app.globalData.isLogin);
                  wx.navigateBack({
                  });
                }
              })
          }
        });
      } else {
        // session过期
      }

    });
  }
})