// pages/test/index.js
const app = getApp()
const fetch = require('../../utils/fetch.js')
Page({


  /**
   * 页面的初始数据
   */
  data: {
    dataSource:[
      { title: "党务公开" },
      { title: "村(居)务公开" },
      { title: "财务公开" }
    ],
    currTabIndex:1,
    newsDatas: [],
    newsDatasRecord: [
      { "datas": [], "page": 0, "isLoaded": false },
      { "datas": [], "page": 0, "isLoaded": false },
      { "datas": [], "page": 0, "isLoaded": false }
    ],
    hasNews: false,
    region_id: '',
    pageName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.data.region_id = id;
    this.data.pageName = options.name;
    console.log(options.name);

    wx.setNavigationBarTitle({
      title: options.name,
    })
    
    var that = this;
    this.loadNewsInfos(this.data.currTabIndex);
       

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: this.data.pageName,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      //
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('onReachBottom');
    this.loadNewsInfos(this.data.currTabIndex);
  },
  loadNewsInfos: function (news_type) {
    var that = this;
    // 原数据
    var oldDatas = this.data.newsDatasRecord[news_type];

    fetch('yggc/commonApi/getNews', { "code": this.data.region_id, "type": news_type, 'page': oldDatas.page }).then(function (result) {
      console.log(result);
      var code = result.data.code;
      if (code == 200) {

        // 原type=1的数据
        var newDatasRecord = oldDatas;
        // 追加新数据
        newDatasRecord.datas = newDatasRecord.datas.concat(result.data.data);
        //TODO,存储分页信息
        if (result.data.data.length > 0) {
          newDatasRecord.page += 1;
        }
        newDatasRecord.isLoaded = true;
        console.log(newDatasRecord, 'newDatasRecord');
        //存储该记录
        that.data.newsDatasRecord[news_type] = newDatasRecord;
        // 重新渲染页面
        that.setData({
          newsDatas: newDatasRecord.datas,
          hasNews: newDatasRecord.datas.length > 0
        });
      }
    }); 
  },
  toNewsItemDetail: function (e){
    console.log("toNewsItemDetail", e);
    var id = e.detail.id;
    // 传递文章id
    wx.navigateTo({
      url: '../news_detail/index?id=' + id,
    })
  },
  changeTabIndex:function(e){
    var index = e.detail.index;
    this.data.currTabIndex = index;
    var oldDatas = this.data.newsDatasRecord[index];
    if (oldDatas.isLoaded) {
      this.setData({
        newsDatas: oldDatas.datas,
        hasNews: oldDatas.datas.length > 0
      });
    } else {
      this.loadNewsInfos(index);
    }
  },
  toUserCenter: function () {
    wx.navigateTo({
      url: '../usercenter/index'
    })
  },

  toMain:function(){
    wx.navigateBack({
      delta:2
    })
  },
  toReportList: function () {
    wx.navigateTo({
      url: '../report/index'
    })
  }
})