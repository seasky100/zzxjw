//app.js
const fetch = require('/utils/fetch');
App({
  globalData:{
    userInfo:null,
    isLogin:false,
    idcard:'',
  
  },
  onLaunch: function () {
    this.login();

    var idcard = wx.getStorageSync('idcard');
    console.log(idcard,"idcard");
    if(idcard != ''){
      this.globalData.isLogin = true;
      this.globalData.idcard = idcard;
    }else{
      console.log("isEmpty", "idcard");
      this.globalData.isLogin = false;
      this.globalData.idCard = '';
    }

  },
  isLogin:function(){
    return this.globalData.isLogin;
  },

  toLoginOut:function(){
    if (this.globalData.isLogin){
        this.globalData.isLogin = false;
        this.globalData.idcard = "";
        wx.setStorageSync("idcard", '');
    }
  },

  getUserInfo: function (cb) {
    var that = this
    if (this.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //调用登录接口
      wx.login({
        success: function () {
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              console.log(res,"login");
              typeof cb == "function" && cb(that.globalData.userInfo)
            }
          })
        }
      })
    }
  },

  updateUserInfo:function(){
      this.getUserInfo(
        function(userInfo){
          console.log(userInfo,"updateUserInfo");
          var session = wx.getStorageSync('3rd_session_key');
          var userInfoStr = JSON.stringify(userInfo);
          fetch('yggc/commonApi/updateUserinfo', { wx_session: session, userInfo: userInfoStr },"POST");
        }
      );
  },

  /**
   * 获取登录码
   */
  login: function (callback) {
    var that = this;
    var session = wx.getStorageSync('3rd_session_key');
    if (session == '') {
      console.log('session is empty');
      wx.login({
        success: res => {
          console.log(res);
          // 发起网络请求，获取session，
          fetch('yggc/commonApi/login', { wx_code: res.code}).then(function (result) {
            console.log(result);
            var code = result.data.code;
            if (code == 200 || code == 201) {
              var session = result.data.result;
              wx.setStorageSync('3rd_session_key', session);
              if (callback != undefined) {
                  callback(session);
              }
              if (code == 201) {
                that.updateUserInfo();
              }
            }else{
              wx.removeStorageSync('3rd_session_key');
              if (callback != undefined) {
                callback(false);
              }
            }
          });
        }
      });
    } else {
      console.log('session is ok');
      if (callback != undefined) {
          callback(session);
      }
    }

  },


  getBaseUrl:function(){
    var baseUrl = "https://jw.handecms.com/";
    return baseUrl;
  },

  /**
   * 支付金额
   */
  toPay:function(fee,success,fail){
    var that = this;
    this.login(function (session) {
      console.log("session:", session);
      fetch('home/commonApi/wxPay', { session_key: session, fee: fee }).then(
        function (result) {
          console.log('wxPay', result);
          if (result.data.code == 410) {
            wx.removeStorageSync('3rd_session_key');
            that.toPay(fee, success, fail);
          } else {
            var params = result.data.result;
            wx.requestPayment({
              'timeStamp': params.timeStamp,
              'nonceStr': params.nonceStr,
              'package': params.package,
              'signType': 'MD5',
              'paySign': params.paySign,
              'success': function (res) {
                console.log(res);
                if(success != undefined){
                  success(res);
                }
              },
              'fail': function (res) {
                console.log(res);
                if (fail != undefined) {
                  fail(res);
                }
              }
            })
          }
        }
      );
    });
  }
})