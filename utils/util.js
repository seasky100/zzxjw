const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 * 格式年月
 */
const formatMouth = date =>{
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  return [year, month].map(formatNumber).join('-') 
}
/**
 * 格式年月
 */
const formatYear = date => {
  const year = date.getFullYear()
  return year
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

 
// 校验身份证号
const validityIdCard = idCard => {
  const lenght = idCard.length;
  if (lenght != 18){
    return false;
  }

  var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  if (reg.test(idCard) === false) {
    // alert("身份证输入不合法");
    return false;
  }
  return true;
}

module.exports = {
  formatTime: formatTime,
  formatMouth:formatMouth,
  formatYear: formatYear,
  isValidityIdCard: validityIdCard
}
