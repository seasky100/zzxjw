Component({
  // 对外属性，
  properties: {
    news_id:{
      type: String,
      value:''
    },
    // 图片
    icon: {
      type: String,
      value: ''
    },
    //标题
    title: {
      type: String,
      value: '',
    },
    //摘要
    subTitle: {
      type: String,
      value: '',
    },
    //发布日期
    date:{
      type: String,
      value:''
    },
    //阅读总数
    count:{
      type:String,
      value:'',
      observer: function (newVal, oldVal) {
        
        if (this.properties.count > 1000 && this.properties.count < 10000)         {
          this.data.countDesc = "999+";
        } else if (this.properties.count > 10000) {
          this.data.countDesc = "1万 +";
        }else{
          this.data.countDesc = newVal;
        }

        if(this.data.isReady){
          this.reloadCountDesc();
        }
      }
    }
  },
  data: {
      countDesc:'',
      isReady:false
  },

  ready:function(){

    this.data.isReady = true;
    this.setData({
      countDesc:this.data.countDesc
    }
    );
  },
  methods: {
    reloadCountDesc:function(){
      this.setData({
        countDesc: this.data.countDesc
      }
      );
    },
    onClick: function () {
      console.log(this.properties,'onClick');
      var myEventDetail = { "id": this.properties.news_id };
      // detail对象，提供给事件监听函数
      var myEventOption = { bubbles: true, composed: true }; // 触发事件的选项
      this.triggerEvent('clickListenner', myEventDetail, myEventOption);
    },
    imageLoadError:function(){
      console.log("imageLoadError");
      this.setData({
        icon: '/images/scan.png'
      }) 
    }

  }
})