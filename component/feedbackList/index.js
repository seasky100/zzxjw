// component/feedbackList/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    datas:{
        type: Array,
        value: []
    },
    userIcon:{
      type:String,
      value:""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    audio_id:0,
    audio_src:''
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
