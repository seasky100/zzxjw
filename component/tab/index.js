// component/tab/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    dataSource: {
      type: Array,
      value: [
        { title: "党务公开" },
        { title: "政务公开" },
        { title: "财务公开" }]
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
      currSelected:1,
      
  },

  ready: function () {
      var query = wx.createSelectorQuery();
      var item = this.selectComponent("#item_"+this.data.currSelected);
      console.log(item);
  },
  /**
   * 组件的方法列表
   */
  methods: {
    onSelected: function (e) {
      var index = e.target.dataset.index;
      this.setData({
        currSelected:index
        });

      var myEventDetail = { "index": index };
      // detail对象，提供给事件监听函数
      var myEventOption = { bubbles: true, composed: true }; // 触发事件的选项
      this.triggerEvent('selectListenner', myEventDetail, myEventOption);
    }
  }
})
