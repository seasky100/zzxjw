Component({
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    merchant_icon: {
      type: String,
      value: '',
    },
    merchant_name: {
      type: String,
      value: '',
    }
  },
  data: {
    noPrice:true,
    price:""
  },
  methods: {
    /**
     * 内部方法，监听金额变化，修改按钮状态
     */
    on_PB_Changed:function(e){
      // console.log(e);
      var v = e.detail.value;
      var arr = v.split('.');
      // console.log(arr);
      if(arr.length==2){
        var s = arr[1];
        if(s.length >2){
          this.data.price = arr[0] + "." + s[0] + s[1];
          return arr[0]+"."+s[0]+s[1];
        }
      }else if(arr.length == 3){
        this.data.price = arr[0] + "." + arr[1];
        return arr[0] + "." +arr[1];
      }

      if(v != ""){
        var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
        var flag = reg.test(v);
        this.setData({
          noPrice: !flag
        });
        this.data.price = v;
      }else{
       
        this.setData({
          noPrice: true
        });
        this.data.price = "";
        return "";
      }  


    },
    _to_PB_pay:function(){
      console.log(this.data.price);
      var myEventDetail = { "price": this.data.price } 
      // detail对象，提供给事件监听函数
      var myEventOption = { bubbles: true, composed:true}; // 触发事件的选项
      this.triggerEvent('onPayListenner', myEventDetail, myEventOption);
    }
    
  }
})