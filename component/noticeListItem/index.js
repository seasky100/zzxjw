// component/noticeListItem/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    notice_id: {
      type: String,
      value: '0'
    },
    article_id:{
      type: String,
      value: '0'
    },
    //标题
    title: {
      type: String,
      value: '默认标题',
    },
    //发布日期
    date: {
      type: String,
      value: '默认时间'
    },
    isRead: {
      type: Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClick: function () {
      console.log(this.properties, 'onClick');
      var myEventDetail = { "id": this.properties.notice_id, "article_id": this.properties.article_id };
      // detail对象，提供给事件监听函数
      var myEventOption = { bubbles: true, composed: true }; // 触发事件的选项
      this.triggerEvent('clickListenner', myEventDetail, myEventOption);
    }
  }
})
